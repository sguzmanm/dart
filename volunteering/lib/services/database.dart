import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:volunteering/models/profile.dart';
import 'package:volunteering/models/user.dart';


class DatabaseService {

  final String uid;
  DatabaseService({this.uid});

//collection reference
final CollectionReference userCollection = Firestore.instance.collection('users');

Future updateUserData(String birthDate, String description, String firstName, String lastName, String userName) async {
  return await userCollection.document(uid).setData({
    'birthDate': birthDate,
    'description': description,
    'firstName': firstName,
    'lastName': lastName,
    'userName': userName,
  }); 
}


//lista de perfiles con snapshot
List<Profile> _profileListFromSnapshot(QuerySnapshot snapshot){
  return snapshot.documents.map((doc){
    return Profile(
      birthDate: doc.data['birthDate'] ?? '',
      description: doc.data['description'] ?? '',
      firstName: doc.data['firstName'] ?? '',
      lastName: doc.data['lastName'] ?? '',
      userName: doc.data['userName'] ?? '',
    );
  }).toList();
}


//user data from snapshot
UserData _userDataFromSnapshot(DocumentSnapshot snapshot){
  return UserData(
    uid: uid,
    birthDate: snapshot.data['birthDate'],
    description: snapshot.data['description'],
    firstName: snapshot.data['firstName'],
    lastName: snapshot.data['lastName'],
    userName: snapshot.data['userName'], 
  );
}

// trae la lista de usuarios
Stream<List<Profile>> get profiles {
 return userCollection.snapshots()
  .map(_profileListFromSnapshot);
  }



 //get user doc stream
Stream<UserData> get userdata{
  return userCollection.document(uid).snapshots()
    .map(_userDataFromSnapshot);
} 
}