import 'package:flutter/material.dart';
import 'package:volunteering/services/auth.dart';

class Register extends StatefulWidget {
 
 final Function toggleView;
 Register({this.toggleView});
 
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();

  //campo de texto
  String email='';
  String password='';
  String error ='';

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.green,
          elevation: 0.0,
          title: Text('Volunteering Hub'),
          actions: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.person),
              label: Text('Sign In'),
              onPressed: () {
                widget.toggleView();
              },
            )
          ],
        ),  
        body: ListView(
          children: <Widget>[
          Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formkey,
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Email',
                    focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                  ),
                  
                  validator: (val) => val.isEmpty ? 'Enter an email' : null,
                  onChanged: (val) {
                    setState(() => email = val);                     
                  }
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Password',
                    focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                  ),
                  obscureText: true,
                  validator: (val) => val.length < 6 ? 'Enter a password mayor de 6 caracteres' : null,
                  onChanged: (val){
                    setState(() => password = val); 
                  }
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  color: Colors.green,
                  child: Text(
                    'Register',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                   if(_formkey.currentState.validate()){
                    dynamic result = await _auth.registerWithEmailAndPassword(email, password);
                    if(result == null){
                      setState(() => error = 'Please supply a valid email');
                    } 
                   }
                  },
                ),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0)
                ),
              ],
            ),
          ),
        ),
          ]),   
    );
  }
}