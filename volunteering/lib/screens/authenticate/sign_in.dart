import 'package:flutter/material.dart';
import 'package:volunteering/services/auth.dart';

class SignIn extends StatefulWidget {
 
 final Function toggleView;
 SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();

  
  //campo de texto
  String email='';
  String password='';
  String error ='';



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.green,
          elevation: 0.0,
          title: Text('Volunteering Hub'),
          actions: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.person),
              label: Text('Register'),
              onPressed: () {
                widget.toggleView();
              },
            )
          ],
        ),  
        body: ListView(
          children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formkey,
            child: Column(
              children: <Widget>[
                Container(
                  width:450,
                  height:100,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('lib/assets/mountain.jpg'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Email',
                    focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                  ),
                  validator: (val) => val.isEmpty ? 'Enter an email' : null,
                  onChanged: (val) {
                    setState(() => email = val);                     
                  }
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Password',
                    focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width:2.0)
                    ),
                  ),
                  obscureText: true,
                  validator: (val) => val.length < 6 ? 'Enter a password mayor de 6 caracteres' : null,
                  onChanged: (val){
                    setState(() => password = val); 
                  }
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  color: Colors.green,
                  child: Text(
                    'sign in',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                     if (_formkey.currentState.validate()){
                       dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                       if(result == null){
                         setState(() => error = 'Could not sign in with those credentials');
                       } 
                     }
                  },
                ),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0)
                ),
              ],
            ),
          ),
        ), 
          ]
        ),  
    );
  }
}