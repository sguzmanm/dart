import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/services/database.dart';
import 'package:volunteering/shared/constants.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  
  final _formKey = GlobalKey<FormState>();

  String _currentFirstName;
  String _currentLastName;
  String _currentUserName;
  String _currentBirthDate;
  String _currentDescription;


  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userdata,
      builder: (context, snapshot) {
        if(snapshot.hasData){ 
          UserData userData = snapshot.data;
          return Form(
          key: _formKey,
          child: ListView(
           children: <Widget>[Column(
            children: <Widget>[
              Text(
                'Update your profile information',
                style: TextStyle(fontSize: 18.0),
              ),
              SizedBox(height: 5.0),
              TextFormField(
                initialValue: userData.firstName,
                decoration: textInputDecoration,
                validator: (val) => val.isEmpty  ? 'Please enter first name' : null,
                onChanged: (val) => setState(() => _currentFirstName = val),
              ),
              SizedBox(height: 5.0),
              TextFormField(
                initialValue: userData.lastName,
                decoration: textInputDecoration,
                validator: (val) => val.isEmpty  ? 'Please enter last name' : null,
                onChanged: (val) => setState(() => _currentLastName = val),
              ),
              SizedBox(height: 5.0),
              TextFormField(
                initialValue: userData.userName,
                decoration: textInputDecoration,
                validator: (val) => val.isEmpty  ? 'Please enter user name' : null,
                onChanged: (val) => setState(() => _currentUserName = val),
              ),
              SizedBox(height: 5.0),
              TextFormField(
                initialValue: userData.birthDate,
                decoration: textInputDecoration,
                validator: (val) => val.isEmpty  ? 'Please enter Birth date' : null,
                onChanged: (val) => setState(() => _currentBirthDate = val),
              ),
              SizedBox(height: 5.0),
              TextFormField(
                initialValue: userData.description,
                decoration: textInputDecoration,
                validator: (val) => val.isEmpty  ? 'Please enter Description' : null,
                onChanged: (val) => setState(() => _currentDescription = val),
              ),
              RaisedButton(
                color:  Colors.green,
                child: Text(
                  'update',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async{
                  if(_formKey.currentState.validate()){
                    await DatabaseService(uid: user.uid).updateUserData(
                      _currentBirthDate ?? userData.birthDate,
                      _currentDescription ?? userData.description,
                      _currentFirstName ?? userData.firstName,
                      _currentLastName ?? userData.lastName,
                      _currentUserName ?? userData.userName
                      );
                      Navigator.pop(context);
                  }
                },
              )
            ],
          ),
           ]
          ),
          
          );
        }
        else{
          return null;
        }
        
      }
    );
  }
}