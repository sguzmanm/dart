import 'package:flutter/material.dart';
import 'package:volunteering/models/profile.dart';

class ProfileTile extends StatelessWidget {

  final Profile profile;

  ProfileTile({this.profile});

  @override
  Widget build(BuildContext context) {
    var listTile = ListTile(
          leading: CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.black,
          ),
          title: Text(profile.firstName),
          isThreeLine: true,
          dense: false,
          subtitle: Text('\n Last name: ${profile.lastName} \n\n'+ 'Description: ${profile.description} \n\n' + 'Birth Date: ${profile.birthDate}\n\n' + 'User name: ${profile.userName}'),
        
          
        );
    return Padding(
      padding: EdgeInsets.only(top:8.0),
      child: Card(
        margin: EdgeInsets.fromLTRB(20.0, 6.0, 20.0, 0.0),
        child: listTile,
      ),
      
    );
  }
}