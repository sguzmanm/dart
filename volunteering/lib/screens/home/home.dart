import 'package:flutter/material.dart';
import 'package:volunteering/models/profile.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/screens/home/profile_list.dart';
import 'package:volunteering/screens/home/settings_form.dart';
import 'package:volunteering/services/auth.dart';
import 'package:volunteering/services/database.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/screens/home/profilefin.dart';
import 'package:volunteering/screens/home/activities.dart';

class Home extends StatelessWidget{
  
  final AuthService _auth = AuthService();
  
  @override
  Widget build(BuildContext context){

    void _showSettingsPanel(){
      showModalBottomSheet(context: context, builder: (context){
        return Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
          child: SettingsForm(),
        );
      });
    }

    Future navigateToActivities(context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Activities()));
    }

    return StreamProvider<List<Profile>>.value(
          value: DatabaseService().profiles,
          child: Scaffold(
          backgroundColor: Colors.white,
          appBar:  AppBar(
          title: Text('Your Profile'),
          backgroundColor: Colors.green,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () => _showSettingsPanel(), 
            ),
            IconButton(
              icon: Icon(Icons.panorama),
              onPressed: () {
                navigateToActivities(context);
              },  
            ),
            
            FlatButton.icon(
              icon: Icon(Icons.person),
              label: Text('logout'),
              onPressed: () async {
                await _auth.signOut();
              },
            ),
          ],
        ),
        body: ProfileFin(),
      ),
    );
  }
}