import 'package:flutter/material.dart';
import 'package:volunteering/screens/home/profilefin.dart';

class Activities extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    Future navigateToProfile(context) async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileFin()));
    }


    return Scaffold(
      appBar: AppBar(
        title: Text('Activities'),
        //centerTitle: true,
        backgroundColor: Colors.green,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          ),
          IconButton(
            icon: Icon(Icons.person),
            onPressed: () {
                navigateToProfile(context);
              },
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            width:450,
            height:100,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('lib/assets/map.jpg'),
              ),
            ),
          ),
          Row(
            children:<Widget>[ 
              Container(
                margin: EdgeInsets.fromLTRB(20, 5, 10, 10),
                child: Text(
                  'Current Location: Chapinero Alto, Bogotá',
                  style: TextStyle(
                    fontSize: 17.0,
                    fontFamily: 'Roboto',
                  )
                )
              ),
              Icon(
                Icons.info_outline,
                color: Colors.blue,
                size: 20,
              )    
            ]
          ),
          Container(
            margin: EdgeInsets.all(5.0),
            child: Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(15, 5, 0, 10),
                    child: Text(
                      'Dog care for a day',
                      style: TextStyle(
                        fontSize: 23.0,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.bold,
                      )
                    )
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width:120,
                        height:120,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('lib/assets/puppy.jpg'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          'We are looking for dog loving volunteers, who have spare time on a Sunday morning. We have around 20 puppies looking for a new home that need to be supervised.',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 15,
                          )
                        )
                      ),
                    ]
                  ),
                  Row(children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 0, 0, 7),
                      child: Icon(
                        Icons.location_on,
                        color: Colors.grey,
                        size: 25,
                      )
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(2, 0, 180, 5),
                      child: Text(
                        'Usaquén',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 16,
                        )
                      )
                    ),
                    FlatButton(
                      onPressed: (){},
                      color: Colors.green[300],
                      child: Text(
                        'Learn more',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Roboto',
                        ),
                      )
                    ),
                  ],)
                ],
              )
            )
          ),
          Container(
            margin: EdgeInsets.all(5.0),
            child: Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(15, 5, 0, 10),
                    child: Text(
                      'Building homes marathon',
                      style: TextStyle(
                        fontSize: 23.0,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.bold,
                      )
                    )
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width:120,
                        height:120,
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('lib/assets/house.jpg'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          'In the southern part of Bogotá, we plan to build at least three houses in one day for several of the most vulnerable families in the city. Be sure to bring the right equipment.',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 15,
                          )
                        )
                      ),
                    ]
                  ),
                  Row(children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 0, 0, 7),
                      child: Icon(
                        Icons.location_on,
                        color: Colors.grey,
                        size: 25,
                      )
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(2, 0, 140, 5),
                      child: Text(
                        'Ciudad Bolivar',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 16,
                        )
                      )
                    ),
                    FlatButton(
                      onPressed: (){},
                      color: Colors.green[300],
                      child: Text(
                        'Learn more',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Roboto',
                        ),
                      )
                    ),
                  ],)
                ],
              )
            )
          )
        ]
      ),
    );
  }
}
