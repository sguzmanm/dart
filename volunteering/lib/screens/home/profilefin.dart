import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/models/profile.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/screens/home/profile_tile.dart';
import 'package:volunteering/services/database.dart';


class ProfileFin extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfileFin>  {

  final _formKey = GlobalKey<FormState>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userdata,
      builder: (context, snapshot) {
        if(snapshot.hasData){ 
          UserData userData = snapshot.data;
          return Form(
          key: _formKey,
          child:  
        ListView(
        children: <Widget>[ 
         Container(
          child: Column(
            children: <Widget>[
              Container(
                child:Align(
                alignment: Alignment.topCenter,
                  child: Text(
                    'Welcome Back '+ userData.firstName+'!',
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                    )  
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Container(
                          width:140,
                          height:140,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage('lib/assets/profilepic.jpg'),
                            ),
                          ),
                        ),
                        FlatButton(
                          onPressed: (){},
                          child: Text(
                            'Change profile picture',
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Roboto',
                              color: Colors.lightBlue,
                            ),
                          )
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(20, 0, 0, 15),
                          child:Align(
                          alignment: Alignment.topLeft,
                            child: Text(
                              'Score',
                              style: TextStyle(
                                fontSize: 25.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Roboto',
                              ),  
                            ),
                          ),
                        ),
                        Container(
                          //margin: EdgeInsets.fromLTRB(20, 25, 20, 0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.green, width: 5),
                            shape: BoxShape.circle,
                            
                            //color: Colors.green,
                          ),
                          child: 
                            Padding(
                              padding: EdgeInsets.all(15.0),
                              child: Text(
                                '5.0',
                                style: TextStyle(
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Roboto',
                                ),
                              ),
                            ),
                        ),
                      ], 
                    ),
                  ),
                ],
              ),
              Divider(
                height: 3,
                thickness: 2.5, 
                indent: 20,
                endIndent: 20,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Basic Information',
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      )
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Username: '+userData.userName,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Roboto',
                    )
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Birth date: '+userData.birthDate,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Roboto',
                    )
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Your experience',
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      )
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    userData.description,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Roboto',
                    )
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 0, 5),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Medals',
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      )
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Icon(
                      Icons.stars,
                      color: Colors.orange,
                      size: 35,
                    )
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Longest dog adoption spree',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: 'Roboto',
                        )
                      ),
                      Text(
                        'First place',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: 'Roboto',
                          color: Colors.grey,
                        )
                      ),
                    ]
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Icon(
                      Icons.stars,
                      color: Colors.orange,
                      size: 35,
                    )
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 7, 0, 0),
                        child: Text(
                          'Builded a home in one day',
                          style: TextStyle(
                            fontSize: 15.0,
                            fontFamily: 'Roboto',
                          )
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: Text(
                          'Third place',
                          style: TextStyle(
                            fontSize: 15.0,
                            fontFamily: 'Roboto',
                            color: Colors.grey,
                          )
                        ),
                      ),
                    ]
                  )
                ],
              ),
              Divider(
                height: 3,
                thickness: 2.5, 
                indent: 20,
                endIndent: 20,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 10, 0, 5),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Interests',
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto',
                      )
                  ),
                ),
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: 30,
                      height: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage('lib/assets/bici.jpg'),
                        ),
                      ),
                    )
                  ),
                  Expanded(
                    child: Container(
                      width: 30,
                      height: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage('lib/assets/mountain.jpg'),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      width: 30,
                      height: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage('lib/assets/616408.png'),
                        ),
                      ),
                    ),
                  ),
                ], 
              ),
            ],
          ),
        ),
        ]
        )
      );
 
}
  }
    );
    }
}