class User {

  final String uid;

  User({ this.uid });
}

class UserData {

  final String uid;
  final String birthDate;
  final String description;
  final String firstName;
  final String lastName;
  final String userName;

  UserData({ this.uid, this.birthDate,this.description,this.firstName,this.lastName,this.userName});
 

}