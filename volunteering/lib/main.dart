import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:volunteering/models/user.dart';
import 'package:volunteering/screens/wrapper.dart';
import 'package:volunteering/services/auth.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        home: Wrapper(),
        ),
    );
  }
}
